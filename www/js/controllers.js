var getCorners = function(lat, long) {
  var corners = [];
  lat = Math.round(lat * 100);
  long = Math.round(long * 100);
  corners.push([lat / 100, long / 100]);
  corners.push([lat / 100, (long + 1) / 100]);
  corners.push([(lat + 1) / 100, long / 100]);
  corners.push([(lat + 1) / 100, (long + 1) / 100]);
  return corners;
};

var plotSquare = function($scope, $cordovaGeolocation) {
  $scope.text += '1 \n<br>';
  var lat = 12.9048879;
  var long = 77.5410247;
  // var lat = 12.8899999;
  // var long = 77.5410247;
  var corners = getCorners(lat, long);
  var latLng = new google.maps.LatLng(lat, long);
  var mapOptions = {
    center: latLng,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
    $scope.text += '2 \n<br>';
  $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
    $scope.text += '3 \n<br>';
  google.maps.event.addListenerOnce($scope.map, 'idle', function() {
    $scope.text += '4 \n<br>';
    var marker = new google.maps.Marker({
        map: $scope.map,
        animation: google.maps.Animation.DROP,
        position: new google.maps.LatLng(lat, long)
    });

    // Add markers on the four corners of the square
    for (var i = 0; i < corners.length; i++) {
      var corner = new google.maps.LatLng(corners[i][0], corners[i][1])
      var cornerMarker =  new google.maps.Marker({
        map: $scope.map,
        animation: google.maps.Animation.DROP,
        position: corner
      });
    }

    var bounds = {
      b: {
        b: corners[0][1],
        f: corners[1][1]
      },
      f: {
        b: corners[2][0],
        f: corners[0][0]
      }
    }
    console.log(corners)
    console.log(bounds)
    var currentRectangle = new google.maps.Rectangle();
    currentRectangle.setOptions({
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: $scope.map,
      bounds: {
        north: corners[0][0],
        south: corners[2][0],
        east: corners[1][1],
        west: corners[0][1]
      }
    });
  });
};

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('CluesCtrl', function($scope, $http) {
  $scope.clues = [];
  // $http.get('http://127.0.0.1:5000/clues')
  $http.get('http://192.168.1.50:5000/clues')
    .success(function(resp) {
      $scope.clues = resp;
    })
    .error(function(err, status) {
      console.log('controller.CluesCtrl.get: ', err, status);
    })
})

.controller('GpsCtrl', function($scope, $cordovaGeolocation) {
  $scope.positionAvailable = false;
  $scope.positionMessage = 'Waiting for GPS';
  var posOptions = { timeout: 10000, enableHighAccuracy: false };

  $cordovaGeolocation
  .getCurrentPosition(posOptions)
    .then(function (position) {
      $scope.position = position;
      $scope.positionMessage = 'Co-ordinates found'
      $scope.positionAvailable = true;
      var lat  = position.coords.latitude;
      var long = position.coords.longitude;

      var latLng = new google.maps.LatLng(lat, long);
      var mapOptions = {
        center: latLng,
        zoom: 17,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
      google.maps.event.addListenerOnce($scope.map, 'idle', function() {
        var marker = new google.maps.Marker({
            map: $scope.map,
            animation: google.maps.Animation.DROP,
            position: latLng
        });

        var infoWindow = new google.maps.InfoWindow({
            content: "Current position!"
        });

        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open($scope.map, marker);
        });
      });

    }, function(err) {
      $scope.positionMessage = 'Couldn\'t get co-ordinates: ' + JSON.stringify(err)
      console.log(err)
    });
})

.controller('MapCtrl', function($scope, $cordovaGeolocation) {
  $scope.positionAvailable = true;
  $scope.text = 'started \n<br>';
  $scope.$on('$ionicView.beforeEnter', function(a, b) {
    $scope.text += 'before \n<br>';
    plotSquare($scope, $cordovaGeolocation);
  });
  $scope.$on('$ionicView.afterLeave', function(a, b) {
    document.getElementById('map').innerHTML = '';
  });
})

.controller('CamCtrl', function($scope, $cordovaCamera) {
  // console.log(1)
  // document.addEventListener("deviceready", function () {
  //   alert(2)
  //   try {
  //     alert(Camera)
  //   }
  //   catch (e) {
  //     alert('not defined')
  //   }
  //   var options = {
  //     quality: 50,
  //     destinationType: Camera.DestinationType.DATA_URL,
  //     sourceType: Camera.PictureSourceType.CAMERA,
  //     allowEdit: true,
  //     encodingType: Camera.EncodingType.JPEG,
  //     targetWidth: 100,
  //     targetHeight: 100,
  //     popoverOptions: CameraPopoverOptions,
  //     saveToPhotoAlbum: false,
  //     correctOrientation:true
  //   };
  //   alert(3)

  //   $cordovaCamera.getPicture(options).then(function(imageData) {
  //     var image = document.getElementById('myImage');
  //     image.src = "data:image/jpeg;base64," + imageData;
  //   }, function(err) {
  //     // error
  //   });

  // });
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});

  // var watchOptions = {timeout : 10000, maximumAge: 1, enableHighAccuracy: true };
  // var watch = $cordovaGeolocation.watchPosition(watchOptions);

  // watch.then(
  //   null,
  
  //   function(err) {
  //     console.log(err)
  //   },
  
  //   function(position) {
  //     var lat  = position.coords.latitude
  //     var long = position.coords.longitude
  //     console.log(lat + ' ' + long)
  //     watch.clearWatch();
  //   }
  // );
